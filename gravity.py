''' Author: Bauskar Nikhil '''
''' eMail: nikhil.bauskar@student.tu-freiberg.de '''

from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import json
# tqdm is used for a progress bar
from tqdm import tqdm

# reading data from json file
fileObj = open("input.json",'r')
data = json.load(fileObj)
fileObj.close()

# parameters
GRAVITATIONAL_CONSTANT = data["GRAVITATIONAL_CONSTANT"]  # m^3 kg^-1 s^-2
POSITIONS = np.array(data["POSITIONS"])
VELOCITIES = np.array(data["VELOCITIES"])
TIME_STEP = data["TIME_STEP"]  # s
NUMBER_OF_TIME_STEPS = data["NUMBER_OF_TIME_STEPS"]
PLOT_INTERVAL = data["PLOT_INTERVAL"]
number_of_dimensions = data["number_of_dimensions"]

# derived variables
MASSES = np.array([4 / GRAVITATIONAL_CONSTANT, 4 / GRAVITATIONAL_CONSTANT])
number_of_planets = len(POSITIONS)

# make sure the number of planets is the same for all quantities
assert len(POSITIONS) == len(VELOCITIES) == len(MASSES)
assert POSITIONS.shape[1] == VELOCITIES.shape[1] == number_of_dimensions

def gravityForce(GRAVITATIONAL_CONSTANT, MASSES, POSITIONS):

    x_start, x_end = -1.5, 1.5
    y_start, y_end = -1.5, 1.5
    n_pixel_x, n_pixel_y = 10, 10

    #size of pixel
    dx = (x_end-x_start)/n_pixel_x
    dy = (y_end-y_start)/n_pixel_y

    #1d array with coordinates of vertices
    xv = np.linspace(x_start,x_end,n_pixel_x+1)
    yv = np.linspace(y_start,y_end,n_pixel_y+1)

    #1d array with coordinates of vertices center
    xc = np.linspace(x_start+dx/2,x_end-dx/2,n_pixel_x)
    yc = np.linspace(y_start+dy/2,y_end-dy/2,n_pixel_y)

    #create 2d coordinate array
    xv_2d, yv_2d = np.meshgrid(xv,yv)
    xc_2d, yc_2d = np.meshgrid(xc,yc)

    #get values at centre points
    force_x = np.zeros_like(xc_2d)
    force_y = np.zeros_like(yc_2d)
    for i in range(len(POSITIONS)):
        X = xc_2d - POSITIONS[i,0]
        Y = yc_2d - POSITIONS[i,1]
        #potential += -GRAVITATIONAL_CONSTANT*MASSES[i]/np.sqrt(X**2+Y**2)
        force_x += GRAVITATIONAL_CONSTANT*MASSES[i]*X/(X**2+Y**2)**1.5
        force_y += GRAVITATIONAL_CONSTANT*MASSES[i]*Y/(X**2+Y**2)**1.5
    
    return xc_2d, yc_2d, force_x, force_y

for step in tqdm(range(NUMBER_OF_TIME_STEPS + 1)):
    # plotting every single configuration does not make sense
    if step % PLOT_INTERVAL == 0:
        fig, ax = plt.subplots()
        xv_2d,yv_2d,values_x,values_y = gravityForce(GRAVITATIONAL_CONSTANT, MASSES, POSITIONS)
        x = POSITIONS[:,0]
        y = POSITIONS[:,1]
        ax.scatter(x,y)
        ax.quiver(xv_2d,yv_2d,values_x,values_y)
        ax.set_aspect("equal")
        ax.set_xlim(-1.5, 1.5)
        ax.set_ylim(-1.5, 1.5)
        ax.set_title("Force Distribution @ t = {:8.4f} s".format(step * TIME_STEP))
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        output_file_path = Path("Forces", "{:016d}.png".format(step))
        output_file_path.parent.mkdir(exist_ok=True)
        fig.savefig(output_file_path)
        plt.close(fig)

    # Calculation of distance vector and norm in addition to planets
    distance_vector = POSITIONS[:,None,:]-POSITIONS[None,:,:]
    distance_vector_length = np.linalg.norm(distance_vector, axis=-1)

    # Calculation of total acceleration in addition to planets
    acceleration_contribution = GRAVITATIONAL_CONSTANT * distance_vector * MASSES[:,None] / distance_vector_length[:,:,None]**2
    np.fill_diagonal(acceleration_contribution[:,:,0],0)
    np.fill_diagonal(acceleration_contribution[:,:,1],0)
    accelerations = acceleration_contribution.sum(axis = 0)

    # Updation of Positions and Velocities
    POSITIONS = POSITIONS + (VELOCITIES*TIME_STEP)
    VELOCITIES = VELOCITIES + (accelerations*TIME_STEP)
    
    
